package com.example.config;

import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 *  { @link https://sa-token.dev33.cn/doc/index.html#/use/route-check}
 */
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {
//   注册Sa-Token的拦截器
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    // 注册路由拦截器，自定义认证规则
    registry.addInterceptor(new SaRouteInterceptor((req, res, handler) -> {

//      System.out.println(111+JSONUtil.toJsonStr(req));
//
//      System.out.println(222+JSONUtil.toJsonStr(res));
//
//      System.out.println(333+JSONUtil.toJsonStr(handler));


//       登录认证 -- 拦截所有路由，并排除/user/doLogin 用于开放登录
      SaRouter.match("/**", "/user/doLogin", r -> StpUtil.checkLogin());
//
//      // 角色认证 -- 拦截以 admin 开头的路由，必须具备 admin 角色或者 super-admin 角色才可以通过认证
//      SaRouter.match("/admin/**", r -> StpUtil.checkRoleOr("admin", "super-admin"));
//
//      // 权限认证 -- 不同模块认证不同权限
//      SaRouter.match("/user/**", r -> StpUtil.checkPermission("user"));
//      SaRouter.match("/admin/**", r -> StpUtil.checkPermission("admin"));
//      SaRouter.match("/goods/**", r -> StpUtil.checkPermission("goods"));
//      SaRouter.match("/orders/**", r -> StpUtil.checkPermission("orders"));
//      SaRouter.match("/notice/**", r -> StpUtil.checkPermission("notice"));
//      SaRouter.match("/comment/**", r -> StpUtil.checkPermission("comment"));
//
//      // 甚至你可以随意的写一个打印语句
//      SaRouter.match("/**", r -> System.out.println("----啦啦啦----"));
//
//      // 连缀写法
//      SaRouter.match("/**").check(r -> System.out.println("----啦啦啦----"));

    }))
            .addPathPatterns("/**")
            .excludePathPatterns("/user/doLogin")
    ;




  }



//  @Override
//  public void addInterceptors(InterceptorRegistry registry) {
//    // 注册 Sa-Token 的路由拦截器
//    registry.addInterceptor(new SaRouteInterceptor())
//            .addPathPatterns("/**")
//            .excludePathPatterns("/user/doLogin");
//  }
}
