package com.example.config;

//import cn.hutool.json.JSONUtil;
//import com.example.common.ResponseResult;
//import org.springframework.core.MethodParameter;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.server.ServerHttpRequest;
//import org.springframework.http.server.ServerHttpResponse;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
//
////@ControllerAdvice(annotations = Controller.class)
//public class MyResponseAdvice implements ResponseBodyAdvice<Object> {
//
//  @Override
//  public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
////        判断是否要执行 beforeBodyWrite 方法， false不执行
////        也可以指定哪些方法进行处理
//    return true;
//  }
//
//  @Override
//  public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
//
//    System.out.println("body===>"+body);
//    if (body instanceof ResponseResult) {
////            已经定义好了返回类型
//      return body;
//    } else if(body instanceof String) {
//      //        如果body是String需要单独处理
//      return JSONUtil.toJsonStr(ResponseResult.success(body));
//    } else {
//      return (ResponseResult.success(body));
//    }
//  }
//}