package com.example.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 权限系统的权限组
 * </p>
 *
 * @author zq
 * @since 2022-04-08
 */
@Data
@ApiModel(value = "Permissiontest对象", description = "权限系统的权限组")
public class PermissiontestDto implements Serializable{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("权限组id")
    private String permissionId;

    @ApiModelProperty("权限组名")
    private String name;

    @ApiModelProperty("状态")
    private Integer status;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("功能描述")
    private String remark;

    @ApiModelProperty("创建人")
    private String creator;

    @ApiModelProperty("记录创建时间")
    private Long ctime;

    @ApiModelProperty("记录更新时间")
    private Long utime;

    private String updator;

}
