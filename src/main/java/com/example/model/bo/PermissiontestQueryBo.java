package com.example.model.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description :
 * @Author : zq
 * @Date: 2021-01-20 14:42
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PermissiontestQueryBo implements Serializable {

  @ApiModelProperty("ID")
  private Long id;

  @ApiModelProperty("权限组id")
  private String permissionId;

  @ApiModelProperty("权限组名")
  private String name;

  @ApiModelProperty("状态")
  private Integer status;

  @ApiModelProperty("排序")
  private Integer sort;


}
