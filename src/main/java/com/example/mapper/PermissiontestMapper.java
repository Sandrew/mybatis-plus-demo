package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.Permissiontest;

/**
 * <p>
 * 权限系统的权限组 Mapper 接口
 * </p>
 *
 * @author zq
 * @since 2022-04-08
 */
public interface PermissiontestMapper extends BaseMapper<Permissiontest> {

}
