package com.example.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

  // 测试登录，浏览器访问： http://localhost:8081/user/doLogin?username=zhang&password=123456
  @GetMapping("/doLogin")
  public SaTokenInfo doLogin(String username, String password) {
    // 此处仅作模拟示例，真实项目需要从数据库中查询数据进行比对
    if("zhang".equals(username) && "123456".equals(password)) {
      StpUtil.login(10001);

      SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
      return tokenInfo;
    }
    return null;
  }

  // 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin
  @GetMapping("/isLogin")
  public String isLogin() {
    return "当前会话是否登录：" + StpUtil.isLogin();
  }



  @GetMapping("/logout")
  public void logout() {
      StpUtil.logout();
  }

}
