package com.example.controller;

import cn.hutool.core.bean.BeanUtil;
import com.example.common.ResponseResult;
import com.example.entity.Permissiontest;
import com.example.model.bo.PermissiontestQueryBo;
import com.example.model.dto.PermissiontestDto;
import com.example.service.impl.PermissiontestServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 权限系统的权限组 前端控制器
 * </p>
 *
 * @author zq
 * @since 2022-04-08
 */
@RestController
@RequestMapping("/permissiontest")
public class PermissiontestController {

  @Resource
  private PermissiontestServiceImpl permissiontestService;

  @ApiOperation(value = "list")
  @PostMapping("/list")
  public ResponseResult<List<PermissiontestDto>> list(@RequestBody PermissiontestQueryBo bo){
    List<Permissiontest> permissiontestList = permissiontestService.getLambdaQueryChainWrapper(bo)
            .list();
    List<PermissiontestDto> permissiontestDtos = BeanUtil.copyToList(permissiontestList, PermissiontestDto.class);
    return ResponseResult.success(permissiontestDtos);
  }

}
