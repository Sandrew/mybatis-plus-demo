package com.example.common;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.sql.SQLException;
import java.util.Collections;

/**
 * 本地代码生成器
 * <p>
 * 参考文档:
 * </p>
 *
 * @author https://daughter.qdingnet.com
 */
public class CodeGenerator {


  /**
   * 执行 run
   */
  public static void main(String[] args) throws SQLException {


    CodeGeneratorConfig codeGeneratorConfig = new CodeGeneratorConfig();

    FastAutoGenerator.create("jdbc:mysql://moeenas.myqnapcloud.cn:50011/mybatis_plus_demo", "sandrew", "Zqyzq@mq02")
            .globalConfig(builder -> {
              builder.author("zq") // 设置作者
                      .enableSwagger() // 开启 swagger 模式
                      .fileOverride() // 覆盖已生成文件
                      .outputDir(codeGeneratorConfig.getRootPath() + codeGeneratorConfig.getJavaPath())
              ; // 指定输出目录
            })
            .packageConfig(builder -> {
//              builder.parent(codeGeneratorConfig.getJavaPath()) // 设置父包名
              builder.parent("com.example") // 设置父包名
//                      .moduleName() // 设置父包模块名
                      .pathInfo(Collections.singletonMap(OutputFile.xml, codeGeneratorConfig.getMapperXmlPath()))
              ; // 设置mapperXml生成路径
            })
            .strategyConfig(builder -> {
              builder.addInclude("decorate_config") // 设置需要生成的表名
                      .addTablePrefix("t_", "c_"); // 设置过滤表前缀
            })
            .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
            .execute();

    System.out.println(codeGeneratorConfig.getRootPath());
  }


}
