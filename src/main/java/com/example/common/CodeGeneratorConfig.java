package com.example.common;

import java.io.File;
import java.net.URL;
import java.util.Objects;



public class CodeGeneratorConfig {

  public String getRootPath() {
    String file = ((URL) Objects.requireNonNull(this.getClass().getClassLoader().getResource(""))).getFile();
    return (new File(file)).getParentFile().getParent();
  }


  public String getJavaPath() {
//    String javaPath = this.getRootPath() + "/src/main/java";
    String javaPath = "/src/main/java";
    System.err.println(" Generator Java Path:【 " + javaPath + " 】");
    return javaPath;
  }

  public String getResourcePath() {
//    String resourcePath = this.getRootPath() + "/src/main/resources";
    String resourcePath = "/src/main/resources";
    System.err.println(" Generator Resource Path:【 " + resourcePath + " 】");
    return resourcePath;
  }

  public String getMapperXmlPath() {
//    String resourcePath = this.getRootPath() + "/src/main/resources";
    String resourcePath = this.getRootPath() + "/src/main/resources/mapper";
    System.err.println(" Generator Resource Path:【 " + resourcePath + " 】");
    return resourcePath;
  }



  public String getTestPath() {
    String testPath = this.getRootPath() + "/src/test/java";
    System.err.println(" Generator Test Path:【 " + testPath + " 】");
    return testPath;
  }


}
