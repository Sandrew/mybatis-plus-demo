package com.example.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;


@AllArgsConstructor
@Data
public class ResponseResult<T> implements Serializable {
  private int code = 1000;
  private String msg = "成功";
  private T data;

  //    私有构造器
  private ResponseResult() {
  }

  //    通用成功
  public static ResponseResult success() {
    return success(null);
  }

  public static ResponseResult success(Object data) {
    ResponseResult responseResult = new ResponseResult<>();
    responseResult.setCode(200);
    responseResult.setMsg("");
    responseResult.setData(data);

    return responseResult;
  }

  //    通用失败
  public static ResponseResult error() {
    return error(null);
  }

  public static ResponseResult error(Object data) {
    ResponseResult responseResult = new ResponseResult<>();
    responseResult.setCode(400);
    responseResult.setMsg("error");
    responseResult.setData(data);

    return responseResult;
  }

  //    自定义参数,链式编程
  public ResponseResult data(T data) {
    this.setData(data);
    return this;
  }

  public ResponseResult code(int code) {
    this.setCode(code);
    return this;
  }

  public ResponseResult msg(String msg) {
    this.setMsg(msg);
    return this;
  }

  //    自定义返回数据
  @Override
  public String toString() {
    return "RestResult{" +
            "code=" + code +
            ", msg='" + msg + '\'' +
            ", data=" + data +
            '}';
  }
}