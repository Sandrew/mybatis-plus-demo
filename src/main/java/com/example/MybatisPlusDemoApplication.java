package com.example;

import cn.dev33.satoken.SaManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author zq
 */
@SpringBootApplication
@MapperScan("com.example.mapper")
@EnableOpenApi //Enable open api 3.0.3 spec
public class MybatisPlusDemoApplication {

  public static void main(String[] args) throws JsonProcessingException {
    SpringApplication.run(MybatisPlusDemoApplication.class, args);
    System.out.println("启动成功：Sa-Token配置如下：" + SaManager.getConfig());

  }

}
