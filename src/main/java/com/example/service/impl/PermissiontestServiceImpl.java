package com.example.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.Permissiontest;
import com.example.mapper.PermissiontestMapper;
import com.example.model.bo.PermissiontestQueryBo;
import com.example.service.IPermissiontestService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限系统的权限组 服务实现类
 * </p>
 *
 * @author zq
 * @since 2022-04-08
 */
@Service
public class PermissiontestServiceImpl extends ServiceImpl<PermissiontestMapper, Permissiontest> implements IPermissiontestService {



  public LambdaQueryChainWrapper<Permissiontest> getLambdaQueryChainWrapper(PermissiontestQueryBo bo) {
    return lambdaQuery()
            .eq(null != bo.getId(), Permissiontest::getId, bo.getId())
            .eq(StrUtil.isNotBlank(bo.getName()), Permissiontest::getName, bo.getName())
            .in(null != bo.getStatus(), Permissiontest::getStatus,bo.getStatus())
            ;
  }

}
