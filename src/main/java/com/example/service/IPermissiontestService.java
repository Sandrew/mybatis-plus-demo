package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.Permissiontest;

/**
 * <p>
 * 权限系统的权限组 服务类
 * </p>
 *
 * @author zq
 * @since 2022-04-08
 */
public interface IPermissiontestService extends IService<Permissiontest> {

}
